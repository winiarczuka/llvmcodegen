%namespace llvmcodegen

%union
{
public string  val;
public TreeInfo info;
public Relation relation;
}

%token Print Read Exit Assign Increment Decrement Plus Minus Multiplies Divides OpenPar ClosePar 
%token LessEqual GreaterEqual Less Greater Equals NotEquals And Or Negation True False
%token Endl Eof Comma Error 
%token <val> Ident IntNumber RealNumber StringLiteral
%token <val> Type

%type <relation> relation equalizer
%type <val> mulop addop unaryOperator
%type <info> line exp constant primitive postfix prefix multiplicative additive relational minus
%type <info> assignment equality

%%

start
	:
	{
		Compiler.EmitCode("; linia {0,3} :  "+Compiler.source[lineno-1],lineno);
	}
	line { ++lineno; } lineHack
	;

lineHack
	: start
	| /* empty */
	;
line
	: Print list end
	| Read Ident end
	{
		if(!Compiler.variables.ContainsKey($2))
		{
			Error("  line {0,3}:  error - variable "+$2+" not defined",lineno);
			return;
		}

		var variable = Compiler.variables[$2];
		variable.initialized=true;

		if(variable.type=='b')
		{
			Error("  line {0,3}:  error - cannot read bool variable",lineno);
			return;
		}
		string format = variable.type=='i' ? "intFormat" : "floatFormat";
		Compiler.EmitCode("call i32 (i8*, ...) @scanf(i8* bitcast ([ 3 x i8]* @"+format+" to i8*), "+variable.llvmType+"* %"+variable.name+")");
	}
	| Type Ident end
	{
		if(Compiler.variables.ContainsKey($2))
		{
			Error("  line {0,3}:  error - variable "+$2+" already defined",lineno);
		}
		else
		{
			string llvmType = $1=="real"? "float" : ($1=="bool"? "i1" : "i32");
			char type = $1=="real"? 'f' : ($1=="bool"? 'b' : 'i');
			Compiler.variables.Add($2,new Compiler.VariableInfo(){name=Compiler.GetNextTmp(),type=type,llvmType=llvmType});
			Compiler.EmitCode("%"+Compiler.variables[$2].name+" = alloca "+llvmType);
		}
	}
	| assignment end
	| Exit Endl
	{
		YYACCEPT;
	}
	| Exit Eof
	{
		YYACCEPT;
	}
	| error Endl
	{
		Error("  line {0,3}:  syntax error",lineno);
		yyerrok();
	}
	| error Eof
	{
		Error("  line {0,3}:  syntax error",lineno);
		yyerrok();
	}
	| Eof
	{
		Error("  line {0,3}:  syntax error - unexpected symbol Eof",lineno);
		yyerrok();
		YYABORT;
	}
	| Endl
	;

list
	: exp
	{
		var name = $1.GetValue();
		if($1.type=='i')
			Compiler.EmitCode("call i32 (i8*, ...) @printf(i8* bitcast ([ 3 x i8]* @intFormat to i8*), i32 %"+name+")");
		else if($1.type=='f')
		{
			string casted = Compiler.GetNextTmp();
			Compiler.EmitCode("%"+casted+" = fpext float %"+name+" to double");
			Compiler.EmitCode("call i32 (i8*, ...) @printf(i8* bitcast ([ 3 x i8]* @floatFormat to i8*), double %"+casted+")");
		}
		else if($1.type=='b')
		{
			string label = Compiler.GetNextTmp();
			Compiler.EmitCode("br i1 %"+name+", label %"+label+"t, label %"+label+"f");
			Compiler.EmitCode(label+"t:");
			Compiler.EmitCode("call i32 (i8*, ...) @printf(i8* bitcast ([ 5 x i8]* @trueFormat to i8*))");
			Compiler.EmitCode("br label %"+label+"end");
			Compiler.EmitCode(label+"f:");
			Compiler.EmitCode("call i32 (i8*, ...) @printf(i8* bitcast ([ 6 x i8]* @falseFormat to i8*))");
			Compiler.EmitCode("br label %"+label+"end");
			Compiler.EmitCode(label+"end:");
		}
	}
	listPart
	| StringLiteral
	{
		int count = $1.Length-2;
		var word = Compiler.PrepareString($1);
		string name=Compiler.GetNextTmp();
		Compiler.EmitConstant("@"+name+" = constant ["+word.length+" x i8] c\""+word.data+"\"");
		Compiler.EmitCode("call i32 (i8*, ...) @printf(i8* bitcast (["+word.length+" x i8]* @"+name+" to i8*))");
	}
	listPart
	;
listPart
	: Comma list
	| /* empty */
	;
end
	: Endl
	| Eof
	{
		Error("  line {0,3}:  syntax error - unexpected symbol Eof",lineno);
	}
	;


constant
	: IntNumber
	{
		$$.type='i';
		$$.retName=$1;
		$$.assignable=false;
	}
	| RealNumber
	{
		$$.type='f';
		$$.retName=Compiler.Float2Hex($1);
		$$.assignable=false;
	}
	| False
	{
		$$.type='b';
		$$.retName="false";
		$$.assignable=false;
	}
	| True
	{
		$$.type='b';
		$$.retName="true";
		$$.assignable=false;
	}
	;
primitive
	: Ident
	{
		if(!Compiler.variables.ContainsKey($1))
		{
			Error("  line {0,3}:  error - variable "+$1+" not defined",lineno);
			return;
		}
		else if(!Compiler.variables[$1].initialized)
		{
			Error("  line {0,3}:  error - variable "+$1+" not initialized",lineno);
			return;
		}
		var data=Compiler.variables[$1];
		$$.type=data.type;
		$$.retName=data.name;
		$$.assignable=true;
	}
	| constant
	{
		$$.type=$1.type;
		string name = Compiler.GetNextTmp();
		string zero = $1.type=='f'? "0.0" : "0";
		string val = $1.type!='b' ? $1.retName : ($1.retName=="true" ? "1" : "0");
		string fl = $1.type=='f' ? "f" : "";
		Compiler.EmitCode("%"+name+" =  "+fl+"add "+$1.LlvmType+" "+zero+","+$1.retName);
		$$.retName=name;
		$$.assignable=false;
	}
	| OpenPar exp ClosePar
	{
		$$=$2;
	}
	;

unaryOperator
	: Increment
	{
		$$="";
	}
	| Decrement
	{
		$$="-";
	}
	;
postfix
	: prefix
	{
		$$=$1;
	}
	| postfix unaryOperator
	{
		$$.type=$1.type;
		$$.assignable=false;
		if($1.type=='b')
		{
			Error("  line {0,3}: error - postfix operation not applicable to bool",lineno);
		}
		else if (!$1.assignable)
		{
			Error("  line {0,3}: error - postfix operation operand not modificable"+$1.retName,lineno);
		}
		else
		{
			string name = $1.GetValue();
			string result = Compiler.GetNextTmp();
			string flt = $1.type=='f' ? "f":"";
			string val = $1.type=='i'? "1" : "1.0";
			Compiler.EmitCode("%"+result+" = "+flt+"add "+$1.LlvmType+" "+$2+val+", %"+name);
			$1.Store(result);
			$$.retName=name;
			$$.assignable=false;
		}

	}
	;

prefix
	: primitive
	{
		$$=$1;
	}
	| unaryOperator prefix
	{
		$$=$2;
		if($2.type=='b')
		{
			Error("  line {0,3}: error - prefix decrement not applicable to bool",lineno);
		}
		else if (!$2.assignable)
		{
			Error("  line {0,3}: error - prefix operation operand not incrementable",lineno);
		}
		else
		{
			var name = $2.GetValue();
			string result = Compiler.GetNextTmp();
			string flt = $2.type=='f' ? "f":"";
			string val = $2.type=='i'? "1" : "1.0";
			Compiler.EmitCode("%"+result+" = "+flt+"add "+$2.LlvmType+" "+$1+val+", %"+name);
			$2.Store(result);
		}

	}
	| Negation prefix
	{
		if($2.type!='b')
		{
			Error("  line {0,3}: error - negation of not bool variable",lineno);
		}
		var val = $2.GetValue();
		var name = Compiler.GetNextTmp();
		Compiler.EmitCode("%"+name+" = xor i1 1, %"+val);
		$$.retName=name;
		$$.type='b';
		$$.assignable=false;
		
	}
	;

minus 
	: Minus postfix
		{
			$$=$2;
			$$.assignable=false;
			if($2.type=='b')
			{
				Error("  line {0,3}: error - prefix operation not applicable to bool",lineno);
			}
			else
			{
				var name = $2.GetValue();
				string result = Compiler.GetNextTmp();
				string flt = $2.type=='f' ? "f":"";
				string val = $2.type=='i'? "1" : "1.0";
				Compiler.EmitCode("%"+result+" = "+flt+"mul "+$2.LlvmType+" -"+val+", %"+name);
				$$.retName=result;
			}
		}
	| postfix
	;

mulop
	: Multiplies
	{
		$$="mul";
	}
	| Divides
	{
		$$="div";
	}
	;

multiplicative
	: minus
	{
		$$=$1;
	}
	| multiplicative mulop minus
	{
		if($1.type==$3.type && $1.type!='b')
			$$.type=$1.type;
		else if (($1.type=='b' && $3.type!='b')|| ($3.type=='b' && $1.type!='b'))
		{
			Error("  line {0,3}: error - bool mulop",lineno);
		}
		else
		{
			$$.type='f';
		}
		string name = Compiler.GetNextTmp();
		string operand1 = $1.GetValue();
		if($1.type!=$$.type)
		{
			string result = Compiler.GetNextTmp();
			Compiler.EmitCode("%"+result+" = sitofp "+$1.LlvmType+" %"+operand1+" to "+$$.LlvmType);
			operand1=result;
		}
		string operand2 = $3.GetValue();
		if($3.type!=$$.type)
		{
			string result = Compiler.GetNextTmp();
			Compiler.EmitCode("%"+result+" = sitofp "+$3.LlvmType+" %"+operand2+" to "+$$.LlvmType);
			operand2=result;
		}
		string operation;
		if($2=="mul")
		{
			if($$.type=='f')
				operation="fmul";
			else
				operation="mul";
		}
		else
		{
			if($$.type=='f')
				operation="fdiv";
			else
				operation="sdiv";
		}


		Compiler.EmitCode("%"+name+" = "+operation+" "+$$.LlvmType+" %"+operand1+", %"+operand2);
		$$.retName=name;
		$$.assignable=false;
	}
	;

addop
	: Plus
	{
		$$="add";
	}
	| Minus
	{
		$$="sub";
	}
	;

additive
	: multiplicative
	{
		$$=$1;
	}
	| additive addop multiplicative
	{
		if($1.type==$3.type && $1.type!='b')
			$$.type=$1.type;
		else if (($1.type=='b' && $3.type!='b')|| ($3.type=='b' && $1.type!='b'))
		{
			Error("  line {0,3}: error - bool addop",lineno);
		}
		else
		{
			$$.type='f';
		}
		string name = Compiler.GetNextTmp();
		string operand1 = $1.GetValue();
		if($1.type!=$$.type)
		{
			string result = Compiler.GetNextTmp();
			Compiler.EmitCode("%"+result+" = sitofp "+$1.LlvmType+" %"+operand1+" to "+$$.LlvmType);
			operand1=result;
		}
		string operand2 = $3.GetValue();
		if($3.type!=$$.type)
		{
			string result = Compiler.GetNextTmp();
			Compiler.EmitCode("%"+result+" = sitofp "+$3.LlvmType+" %"+operand2+" to "+$$.LlvmType);
			operand2=result;
		}
		string flt = $$.type=='f' ? "f" : "";
		Compiler.EmitCode("%"+name+" = "+flt+$2+" "+$$.LlvmType+" %"+operand1+", %"+operand2);
		$$.retName=name;
		$$.assignable=false;
	}
	;

relation
	: Less
	{
		$$=Relation.Less;
	}
	| Greater
	{
		$$=Relation.Greater;
	}
	| LessEqual
	{
		$$=Relation.LessEqual;
	}
	| GreaterEqual
	{
		$$=Relation.GreaterEqual;
	}
	;

relational
	: additive
	{
		$$=$1;
	}
	| additive relation additive
	{
		var ret = Compiler.PerformRelation($2,$1,$3);
		if(ret.type=='e')
		{
			Error("  line {0,3}: error - bool relation operant not acceptable",lineno);
		}
		$$=ret;
	}
	;

equalizer
	: Equals
	{
		$$=Relation.Equals;
	}
	| NotEquals
	{
		$$=Relation.NotEquals;
	}
	;	

equality
	: relational
	{
		$$=$1;
	}
	| equality equalizer relational
	{
		var ret = Compiler.PerformRelation($2,$1,$3);
		if(ret.type=='e')
		{
			Error("  line {0,3}: error - bool relation operant not acceptable",lineno);
		}
		$$=ret;
	}
	;

exp
	: equality
	{
		$$=$1;
	}
	| exp
	{
		if($1.type!='b')
		{
			Error("  line {0,3}: error - bool operand epected",lineno);
		}
		$$.retName = Compiler.GetNextTmp();
		string name = $1.GetValue();
		Compiler.EmitCode("%"+$$.retName+" = alloca i1");
		Compiler.EmitCode("store i1 %"+name+", i1* %"+$$.retName);
		Compiler.EmitCode("br i1 %"+name+", label %"+$$.retName+"t, label %"+$$.retName+"end");
		Compiler.EmitCode($$.retName+"t:");
	}
	 And equality
	{
		if($4.type!='b')
		{
			Error("  line {0,3}: error - bool operand epected",lineno);
		}
		string value = $4.GetValue();
		Compiler.EmitCode("store i1 %"+value+", i1* %"+$2.info.retName);
		string name = Compiler.GetNextTmp();
		Compiler.EmitCode("br label %"+$2.info.retName+"end");
		Compiler.EmitCode($2.info.retName+"end:");
		Compiler.EmitCode("%"+name+" = load i1, i1* %"+$2.info.retName);
		$$.retName=name;
		$$.type='b';
		$$.assignable=false;
	}
	| exp 
	{
		if($1.type!='b')
		{
			Error("  line {0,3}: error - bool operand epected",lineno);
		}
		$$.retName = Compiler.GetNextTmp();
		string name = $1.GetValue();
		Compiler.EmitCode("%"+$$.retName+" = alloca i1");
		Compiler.EmitCode("store i1 %"+name+", i1* %"+$$.retName);
		Compiler.EmitCode("br i1 %"+name+", label %"+$$.retName+"end, label %"+$$.retName+"f");
		Compiler.EmitCode($$.retName+"f:");
	}
	Or equality
	{
		if($4.type!='b')
		{
			Error("  line {0,3}: error - bool operand epected",lineno);
		}
		string name = Compiler.GetNextTmp();
		Compiler.EmitCode("br label %"+$2.info.retName+"end");
		Compiler.EmitCode($2.info.retName+"end:");
		Compiler.EmitCode("%"+name+" = load i1, i1* %"+$2.info.retName);
		$$.retName=name;
		$$.type='b';
		$$.assignable=false;
	}
	;

assignment
	: Ident Assign exp
	{
		if(!Compiler.variables.ContainsKey($1))
		{
			Error("  line {0,3}: error - variable "+$1+" not defined",lineno);
			return;
		}
		else
		{
			Compiler.variables[$2.val].initialized=true;
		}
		char identType=Compiler.variables[$2.val].type;
		if(identType!=$3.type)
		{
			if((identType=='b' || $3.type=='b')||(identType=='i'))
			{
				Error("  line {0,3}: error - wrong assign type",lineno);
				return;
			}
			else
				$$.type='f';
		}
		else
		{
			$$.type=identType;
		}

		string name=$3.GetValue();
		if($3.type!=$$.type)
		{
			string result = Compiler.GetNextTmp();
			Compiler.EmitCode("%"+result+" = sitofp "+$3.LlvmType+" %"+name+" to "+$$.LlvmType);
			name=result;
		}
		var info = Compiler.variables[$2.val];
		Compiler.EmitCode("store "+info.llvmType+" %"+name+", "+info.llvmType+"* %"+info.name);
		$$.assignable=true;
		$$.retName=info.name;
	}
	;
%%

int lineno=1;

public Parser(Scanner scanner) : base(scanner) { }

private void Error(string error)
{
	Console.WriteLine(error);
	++Compiler.errors;
}

private void Error(string error, params object[] arg)
{
	if(Compiler.Error(lineno))
	{
	Console.WriteLine(error,arg);
	++Compiler.errors;
	}
}
