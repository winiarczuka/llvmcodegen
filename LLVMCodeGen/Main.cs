﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using QUT.Gppg;
using System.Globalization;

namespace llvmcodegen
{
	public struct TreeInfo
	{
		public char type;
		public string retName;
		public bool assignable;

		public string GetValue()
		{
			if (!assignable)
				return retName;
			else
			{
				string name = Compiler.GetNextTmp();
				Compiler.EmitCode($"%{name} = load {LlvmType}, {LlvmType}* %{retName}");
				return name;
			}
		}

		public void Store(string name)
		{
			if (!assignable)
				name = retName;
			else
				Compiler.EmitCode($"store {LlvmType} %{name}, {LlvmType}* %{retName}");
		}

		public string LlvmType
		{
			get
			{
				switch (type)
				{
					case 'b':
						return "i1";
					case 'i':
						return "i32";
					case 'f':
						return "float";
					default:
						return "i32";
				}
			}
		}
	}

	public enum Relation
	{
		Less,
		Greater,
		LessEqual,
		GreaterEqual,
		Equals,
		NotEquals
	}


	class Compiler
	{

		public static int errors = 0;

		public static List<string> source;

		// arg[0] określa plik źródłowy
		// pozostałe argumenty są ignorowane
		public static int Main(string[] args)
		{
			string file;
			FileStream source;
			Console.WriteLine("\nSingle-Pass LLVM Code Generator - Gardens Point");
			if (args.Length >= 1)
				file = args[0];
			else
			{
				Console.Write("\nsource file:  ");
				file = Console.ReadLine();
			}
			try
			{
				var sr = new StreamReader(file);
				string str = sr.ReadToEnd();
				sr.Close();
				Compiler.source = str.Split(new string[] { "\r\n" }, StringSplitOptions.None).ToList();
				source = new FileStream(file, FileMode.Open);
			}
			catch (Exception e)
			{
				Console.WriteLine("\n" + e.Message);
				return 1;
			}
			Scanner scanner = new Scanner(source);
			Parser parser = new Parser(scanner);
			Console.WriteLine();
			variables = new Dictionary<string, VariableInfo>();
			sw = new StreamWriter(file + ".ll");
			constants = new StringWriter();
			code = new StringWriter();


			try
			{
				parser.Parse();
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
			}
			sw.Write(constants);
			EmitProlog();
			sw.Write(code);

			EmitEpilog();
			sw.Close();

			source.Close();

			if (errors == 0)
			{
				Console.WriteLine("  compilation successful\n");
			}
			else
			{
				Console.WriteLine($"\n  {errors} errors detected\n");
				File.Delete(file + ".ll");
			}
			return errors == 0 ? 0 : 2;
		}

		public static void EmitCode(string instr = null)
		{
			code.WriteLine("	" + instr);
		}

		public static void EmitConstant(string constant)
		{
			constants.WriteLine(constant);
		}
		public static void EmitCode(string instr, params object[] args)
		{
			code.WriteLine("	" + instr, args);
			float p = 10;
			
		}

		public static void EmitProlog()
		{
			sw.WriteLine("@intFormat = constant [ 3 x i8] c\"%d\\00\"");
			sw.WriteLine("@floatFormat = constant [ 3 x i8] c\"%f\\00\"");
			sw.WriteLine("@trueFormat = constant [5 x i8] c\"true\\00\"");
			sw.WriteLine("@falseFormat = constant [6 x i8] c\"false\\00\"");
			sw.WriteLine("@endOfExec = constant [17 x i8] c\"End of execution\\00\"");

			sw.WriteLine("declare i32 @printf(i8 *, ...)");
			sw.WriteLine("declare i32 @scanf(i8 *, ...)");
			sw.WriteLine();
			sw.WriteLine("define void @main()");
			sw.WriteLine("{");
		}

		public static void EmitEpilog()
		{
			sw.WriteLine("	call i32 (i8*, ...) @printf(i8* bitcast ([ 17 x i8]* @endOfExec to i8*))");
			sw.WriteLine("	");
			sw.WriteLine("	ret void");
			sw.WriteLine("}");
		}

		private static StreamWriter sw;

		private static StringWriter constants;
		private static StringWriter code;


		public class VariableInfo
		{
			public string name;
			public string llvmType;
			public char type;
			public bool initialized = false;
		}

		public static Dictionary<string, VariableInfo> variables;
		private static int counter = 0;
		private static int lastError = 0;

		public static bool Error(int lineno)
		{
			if (lastError < lineno)
			{
				lastError = lineno;
				return true;
			}
			return false;	
		}

		public static string GetNextTmp()
		{
			counter++;
			return "tmp" + counter.ToString();
		}

		public class StringData
		{
			public int length;
			public string data;
		}

		public static StringData PrepareString(string str)
		{
			var datastr = str.Substring(1, str.Length - 2);
			int i = 0;
			string magicStr = "MagicString";
			while(datastr.Contains(magicStr))
			{
				magicStr = "MagicString" + i;
				i++;
			}
			datastr = datastr.Replace("\\n", magicStr+"0A");
			datastr = datastr.Replace("\\\"", magicStr + "22");
			datastr = datastr.Replace("\\\\", magicStr + "5C");
			datastr = datastr.Replace("\\", "");
			datastr = datastr.Replace(magicStr, "\\");
			datastr = datastr + "\\00";
			var count = datastr.Count(x => x == '\\');
			return new StringData() { length = datastr.Length - 2 * count, data = datastr };
		}

		public static TreeInfo PerformRelation(Relation relation, TreeInfo operand1, TreeInfo operand2)
		{
			TreeInfo result = new TreeInfo();

			if (operand1.type == operand2.type &&(operand1.type!='b' || (relation==Relation.NotEquals || relation==Relation.Equals)) )
				result.type = operand1.type;
			else if ((operand1.type == 'b' && operand2.type != 'b') || (operand2.type == 'b' && operand1.type != 'b') || (operand1.type=='b' && operand2.type=='b' &&(relation!=Relation.Equals || relation!=Relation.NotEquals)))
			{
				result.type = 'e';
				return result;
			}
			else
			{
				result.type = 'f';
			}
			
			string op1 = operand1.GetValue();
			if (operand1.type != result.type)
			{
				string res = Compiler.GetNextTmp();
				Compiler.EmitCode("%" + res + " = sitofp " + operand1.LlvmType + " %" + op1 + " to " + result.LlvmType);
				op1 = res;
			}
			string op2 = operand2.GetValue();
			if (operand2.type != result.type)
			{
				string res = Compiler.GetNextTmp();
				Compiler.EmitCode("%" + res + " = sitofp " + operand2.LlvmType + " %" + op2 + " to " + result.LlvmType);
				op2 = res;
			}
			string name = Compiler.GetNextTmp();
			Compiler.EmitCode("%" + name + " = " +GetRelation(result.type, relation) + " " + result.LlvmType + " %" + op1 + ", %" + op2);
			result.retName = name;
			result.assignable = false;
			result.type = 'b';
			return result;
		}

		public static string Float2Hex(string fNum)
		{
			return "0x" + BitConverter.DoubleToInt64Bits(float.Parse(fNum, CultureInfo.InvariantCulture)).ToString("X2");
		}

		public static string GetRelation(char type, Relation relation)
		{
			string relOp = type == 'f' ? "fcmp " : "icmp ";
			if(type=='f')
			{
				switch (relation)
				{
					case Relation.Less:
						relOp += "ult";
						break;
					case Relation.Greater:
						relOp += "ugt";
						break;
					case Relation.LessEqual:
						relOp += "ule";
						break;
					case Relation.GreaterEqual:
						relOp += "uge";
						break;
					case Relation.Equals:
						relOp += "ueq";
						break;
					case Relation.NotEquals:
						relOp += "une";
						break;
					default:
						break;
				}
			}
			else
			{
				switch (relation)
				{
					case Relation.Less:
						relOp += "slt";
						break;
					case Relation.Greater:
						relOp += "sgt";
						break;
					case Relation.LessEqual:
						relOp += "sle";
						break;
					case Relation.GreaterEqual:
						relOp += "sge";
						break;
					case Relation.Equals:
						relOp += "eq";
						break;
					case Relation.NotEquals:
						relOp += "ne";
						break;
					default:
						break;
				}
			}
			return relOp;
		}

	}
}
