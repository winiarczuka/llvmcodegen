int uninitialized
int x
x = 0
int y
y = 0
real z
z = 0
bool b1
bool b2
bool b
b = true
real w
w = 0


























x = 64 * 21 - 123 / (21 + 11) + (75 - 23) / 2
print "Folding: ", x, "\n"

z = 324 * 26.0 - 123 / (23.0 + 22) + (123 - 23) / 10
print "Folding: ", z, "\n"

b = 324 * 26.0 - 123 / (23.0 + 22) + (123 - 23) / 10 == 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2
print "Folding: ", b, "\n"

print "Folding: ", 324 * 26.0 - 123 / (23.0 + 22) + (123 - 23) / 10 == 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2, "\n"

b = false && 324 * 26.0 - 123 / (23.0 + 22) + (x - 23) / 10 != 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2
b = true && 324 * 26.0 - 123 / (23.0 + 22) + (x - 23) / 10 != 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2
b = false || 324 * 26.0 - 123 / (23.0 + 22) + (x - 23) / 10 == 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2
b = true || 324 * 26.0 - 123 / (23.0 + 22) + (x - 23) / 10 == 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2

b = 324 * 26.0 - 123 / (23.0 + 22) + (x - 23) / 10 == 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2 && false
b = 324 * 26.0 - 123 / (23.0 + 22) + (x - 23) / 10 == 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2 && true 
b = 324 * 26.0 - 123 / (23.0 + 22) + (x - 23) / 10 == 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2 || false
b = 324 * 26.0 - 123 / (23.0 + 22) + (x - 23) / 10 == 64 * 21.0 - 123 / (21 + 11.0) + (75 - 23) / 2 || 324 * 26.0 - 123 / (123 - 23) / 10 != (21 + 11.0) + (75 - 23) / 2 


bool temporary
temporary = 123 == 100 + 23 && -20 < 100 && (23.0 + 22) + (x - 23) / 10 != 64
bool temporary2
temporary2 = temporary && -900 < -20 && b && 20.0 * -2 > 1 && !!true
b = temporary2  && (b || x == y || 20 != 30)


print "\n\nTesty kodu:\n"
int vi1
int vi2
int vi3
real vr1
real vr2
real vr3
bool vb1
bool vb2
bool vb3

print "\nint op int:\n"
vi1 = 100
vi2 = 5

print vi1, " + ",	vi2, " = ", vi1 + 	vi2, "\n"
print vi1, " - ", 	vi2, " = ", vi1 - 	vi2, "\n"
print vi1, " * ", 	vi2, " = ", vi1 * 	vi2, "\n"
print vi1, " / ", 	vi2, " = ", vi1 / 	vi2, "\n"
print vi1, " == ", 	vi2, " = ", vi1 == 	vi2, "\n"
print vi1, " != ", 	vi2, " = ", vi1 != 	vi2, "\n"
print vi1, " < ", 	vi2, " = ", vi1 < 	vi2, "\n"
print vi1, " <= ", 	vi2, " = ", vi1 <= 	vi2, "\n"
print vi1, " > ", 	vi2, " = ", vi1 > 	vi2, "\n"
print vi1, " >= ", 	vi2, " = ", vi1 >= 	vi2, "\n"

print "\nreal op int:\n"
vr1 = 100
vi2 = 5

print vr1, " + ",	vi2, " = ", vr1 + 	vi2, "\n"
print vr1, " - ", 	vi2, " = ", vr1 - 	vi2, "\n"
print vr1, " * ", 	vi2, " = ", vr1 * 	vi2, "\n"
print vr1, " / ", 	vi2, " = ", vr1 / 	vi2, "\n"
print vr1, " == ", 	vi2, " = ", vr1 == 	vi2, "\n"
print vr1, " != ", 	vi2, " = ", vr1 != 	vi2, "\n"
print vr1, " < ", 	vi2, " = ", vr1 < 	vi2, "\n"
print vr1, " <= ", 	vi2, " = ", vr1 <= 	vi2, "\n"
print vr1, " > ", 	vi2, " = ", vr1 > 	vi2, "\n"
print vr1, " >= ", 	vi2, " = ", vr1 >= 	vi2, "\n"

print "\nint op real:\n"
vi1 = 100
vr2 = 5

print vi1, " + ",	vr2, " = ", vi1 + 	vr2, "\n"
print vi1, " - ", 	vr2, " = ", vi1 - 	vr2, "\n"
print vi1, " * ", 	vr2, " = ", vi1 * 	vr2, "\n"
print vi1, " / ", 	vr2, " = ", vi1 / 	vr2, "\n"
print vi1, " == ", 	vr2, " = ", vi1 == 	vr2, "\n"
print vi1, " != ", 	vr2, " = ", vi1 != 	vr2, "\n"
print vi1, " < ", 	vr2, " = ", vi1 < 	vr2, "\n"
print vi1, " <= ", 	vr2, " = ", vi1 <= 	vr2, "\n"
print vi1, " > ", 	vr2, " = ", vi1 > 	vr2, "\n"
print vi1, " >= ", 	vr2, " = ", vi1 >= 	vr2, "\n"

print "\nreal op real:\n"
vr1 = 100
vr2 = 5

print vr1, " + ",	vr2, " = ", vr1 + 	vr2, "\n"
print vr1, " - ", 	vr2, " = ", vr1 - 	vr2, "\n"
print vr1, " * ", 	vr2, " = ", vr1 * 	vr2, "\n"
print vr1, " / ", 	vr2, " = ", vr1 / 	vr2, "\n"
print vr1, " == ", 	vr2, " = ", vr1 == 	vr2, "\n"
print vr1, " != ", 	vr2, " = ", vr1 != 	vr2, "\n"
print vr1, " < ", 	vr2, " = ", vr1 < 	vr2, "\n"
print vr1, " <= ", 	vr2, " = ", vr1 <= 	vr2, "\n"
print vr1, " > ", 	vr2, " = ", vr1 > 	vr2, "\n"
print vr1, " >= ", 	vr2, " = ", vr1 >= 	vr2, "\n"

print "\nbool op bool:\n"
vb1 = true
vb2 = false

print vb1, " == ",	vb2, " = ", vb1 == 	vb2, "\n"
print vb1, " != ", 	vb2, " = ", vb1 != 	vb2, "\n"
print vb1, " && ", 	vb2, " = ", vb1 && 	vb2, "\n"
print vb1, " || ", 	vb2, " = ", vb1 || 	vb2, "\n"

print "\nunary int:\n"
vi1 = 10

print "-", vi1, " = ", -vi1, "\n"
print "value = ", vi1, "\n" 
 
print "++", vi1, " = ", ++vi1, "\n"
print "value = ", vi1, "\n" 
 
print vi1, "++ = ", vi1++, "\n" 
print "value = ", vi1, "\n" 

print "--", vi1, " = ", --vi1, "\n"
print "value = ", vi1, "\n" 
 
print vi1, "-- = ", vi1--, "\n" 
print "value = ", vi1, "\n" 


print "\nunary real:\n"
vr1 = 10

print "-", vr1, " = ", -vr1, "\n"
print "value = ", vr1, "\n" 
 
print "++", vr1, " = ", ++vr1, "\n"
print "value = ", vr1, "\n" 
 
print vr1, "++ = ", vr1++, "\n" 
print "value = ", vr1, "\n" 

print "--", vr1, " = ", --vr1, "\n"
print "value = ", vr1, "\n" 
 
print vr1, "-- = ", vr1--, "\n" 
print "value = ", vr1, "\n" 


print "\nunary bool:\n"
vb1 = true

print "!", vb1, " = ", !vb1, "\n"
print "value = ", vb1, "\n" 

print "\n\n\n"













real poprawna0nazwa1zmiennej123
x = 123
x = 0123
z = 0000.123
print "z = ", z, "\n"
z = 213
x = -123
x = -0123
z = -0000.123
z = -213
b1 = true
b2 = false
z = x
print b
print z
print x
print 0
print 1.0
print "Test"
print true
print false
print x + -z--
y = x
b = x<z
y = -(++x)
y = ++((x))
y = ++--x
y = ++++(++(x))
y = ++x++ 
y = (++x)++
y = x++ - x++ 
w = z
b = z<x
w = -(++z)
w = ++((z))
w = ++--z
w = ++++(++(z))
w = ++z++ 
w = (++z)++
w = z++ - z++ 
b = !!!!!!!!!!b
b = !!!!!!!!!!true
b = !!!!!!!!!!(x * z - y < 10)
x = x * x
z = z * x
b = z == w
print "\n\nWprowadz int: " 
read x
print "Wprowadzono ", x, "\n"
print "\nWprowadz real: "
read z
print "Wprowadzono ", z, "\n"
print "\nTurbo print ze wszystkim:\n", "nowa linia\n\n", "cudzyslow \"\n", "ukosnik \\\n", "losowy znak \s\n", "Nawet 3 ukosniki \\\\\\\n"
z = w+++x
z = (w++)+x
z = x+(++y)
z = w+ ++x
w = - ++----++----++++----++x++
b = x == y && (123 <= -123 && x != 8.0)


int tt0i
real tt0temp
tt0temp=7.5
tt0i=5
bool tt0b
tt0b = tt0i<=tt0temp
print " tt0b = " , tt0b ,"\n"
bool tt0a
tt0a=false
print tt0a==tt0b, "\n"
int tt0y
tt0y=0
bool tt0c
tt0c= tt0i>10*tt0temp && tt0a==(tt0temp/tt0y>-2)
print " !tt0c = " , !tt0c , "\n"
print 0090 , "\n"
int tt0x
tt0x=2
tt0y = ++((tt0x))
tt0y = ++++(++(tt0x))
print "tt0x " , tt0x , "\n"
print "tt0y " , tt0y , "\n"
int tt0z
tt0z = ++tt0x++
print "tt0x " , tt0x , "\n"
print "tt0z " , tt0z , "\n"
int zmienna1
int zmienna2
int zmienna3
zmienna3=10
zmienna2=50
zmienna1=zmienna2+++zmienna3
zmienna1=zmienna1+(++zmienna2)
zmienna1=zmienna1+ ++zmienna2
x = -(-2)

print "Obliczenia skrocone: \n"
int xg 
xg=0
bool p
p= false && (xg++ + xg++ + xg++ + xg++ >0)
print "xg: ",xg,"\n"
bool g
print "p: ",xg,"\n"
g= true || (xg++ + xg++ + xg++ + xg++ >0)
print "g: ", xg,"\n"
exit





































// Rzeczy po exicie, nie powinny być brane pod uwage
losowe rzeczy ktore nie powinny byc brane pod uwage
adlskjf;das
afgadf'
;lg
s;gadf
kds;aafgd
'ioa;oafg.
'bea
apbt
aafgdetrhagfdaioa




int x
x = 123
real y
bool b
int z
z = 2

y = x
b = x<z
y = -(++x)
y = ++((x))
y = ++--x
y = ++++(++(x))
y = ++x++ 
y = (++x)++
y = x++ - x++ 







int x123
real y

x123 = 123
y = x123

real z

z = x123 + y

z = z * 100 + 123 - 2 + z / y

bool var
var = true && false && true

var = !!!!false

z = ++++x123
z = x123++
z = x123++++
z = -x123

bool var2
var2 = 2 == (3 / 123)

exit

//Undefined variable
x = 123

int x
int y
int a

y = a

x = a - --b

int sadDSsd

exit
int a
a = 2
x = - -y


real abcABC

int i
real i

bool j



y = x
y = x<z
y = -(++x)
y = ++((x))
y = ++--x
y = ++++(++(x))
y = ++x++ 
y = (++x)++
y = x++ - x++ 

abc123 = ++5
abc123 = ++(5)
y = (x+y)++
y = x++++
y = (x++)++
y = ++5
