%using QUT.Gppg;
%namespace llvmcodegen

IntNumber     [0-9]+
RealNumber    [0-9]+\.[0-9]+
Ident         [a-z]([a-z0-9])*
StringLiteral \"(\\.|[^\\"\n])*\"

%%

"print"         { return (int)Tokens.Print; }
"read"          { return (int)Tokens.Read; }
"exit"          { return (int)Tokens.Exit; }
"int"           { yylval.val=yytext; return (int)Tokens.Type; }
"bool"          { yylval.val=yytext; return (int)Tokens.Type; }
"real"          { yylval.val=yytext; return (int)Tokens.Type; }
"false"         { return (int)Tokens.False; }
"true"          { return (int)Tokens.True; }
{IntNumber}     { yylval.val=yytext; return (int)Tokens.IntNumber; }
{RealNumber}    { yylval.val=yytext; return (int)Tokens.RealNumber; }
{Ident}         { yylval.val=yytext; return (int)Tokens.Ident; }
{StringLiteral} { yylval.val=yytext; return (int)Tokens.StringLiteral; }
"++"            { return (int)Tokens.Increment; }
"--"            { return (int)Tokens.Decrement; }
"=="            { return (int)Tokens.Equals; }
"!="            { return (int)Tokens.NotEquals; }
"="             { return (int)Tokens.Assign; }
"+"             { return (int)Tokens.Plus; }
"-"             { return (int)Tokens.Minus; }
"*"             { return (int)Tokens.Multiplies; }
"/"             { return (int)Tokens.Divides; }
"("             { return (int)Tokens.OpenPar; }
")"             { return (int)Tokens.ClosePar; }
"<="            { return (int)Tokens.LessEqual; }
">="            { return (int)Tokens.GreaterEqual; }
"<"             { return (int)Tokens.Less; }
">"             { return (int)Tokens.Greater; }
"&&"            { return (int)Tokens.And; }
"||"            { return (int)Tokens.Or; }
"!"             { return (int)Tokens.Negation; }
"\r"            { return (int)Tokens.Endl; }
<<EOF>>         { return (int)Tokens.Eof; }
","             { return (int)Tokens.Comma; }
" "             { }
"\t"            { }
.               { return (int)Tokens.Error; }